#!/bin/bash
set -e
pwd
ls
export CURRENT_GNUHEALTH_VERSION=$1
cd dist/
gpg --detach-sign -a gnuhealth-all-modules-${CURRENT_GNUHEALTH_VERSION}.tar.gz
gpg --detach-sign -a gnuhealth_all_modules-${CURRENT_GNUHEALTH_VERSION}-py3-none-any.whl
sha256sum gnuhealth-all-modules-${CURRENT_GNUHEALTH_VERSION}.tar.gz > gnuhealth-all-modules-${CURRENT_GNUHEALTH_VERSION}.tar.gz.sha256
sha256sum gnuhealth_all_modules-${CURRENT_GNUHEALTH_VERSION}-py3-none-any.whl > gnuhealth_all_modules-${CURRENT_GNUHEALTH_VERSION}-py3-none-any.whl.sha256
